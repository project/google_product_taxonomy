<?php

/**
 * Batch processor.
 */
function google_product_taxonomy_batch_import_process($line, $vocabulary, &$context) {
  list($google_category_id, $term_path) = explode(' - ', $line);

  if (!drupal_is_cli()) {
    $context['message'] = t('Processing @term_name', array(
      '@term_name' => $term_path,
    ));
  }

  $term_path_elements = explode(' > ', $term_path);
  $term_name = array_pop($term_path_elements);
  $parent_path = implode(' > ', $term_path_elements);

  // Store the category details in $context['results'] to make it available
  // to the later batch operations ($context['sandbox'] is not passed between
  // the operations).
  $context['results']['tree'][$term_path] = array(
    'full_path' => $term_path,
    'term_name' => $term_name,
    'parent_path' => $parent_path,
  );

  // Check if the category (taxonomy term) already exists in the database.
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'taxonomy_term', '=')
    ->entityCondition('bundle', 'google_product_taxonomy', '=')
    ->propertyCondition('vid', $vocabulary->vid, '=')
    ->fieldCondition('google_product_taxonomy_id', 'value', $google_category_id, '=')
    ->execute();

  // Term was found, let's update it if needed.
  if (!empty($entities['taxonomy_term'])) {
    // Load full term entity and update its values.
    $tids = array_keys($entities['taxonomy_term']);
    $term = taxonomy_term_load(reset($tids));
    $term->name = $term_name;
    $term->google_product_taxonomy_id[LANGUAGE_NONE] = array(array('value' => $google_category_id));
    $term->google_product_taxonomy_path[LANGUAGE_NONE] = array(array('value' => $term_path));
    taxonomy_term_save($term);
  }
  // Term was not found, let's create a new one.
  else {
    $term = (object) array(
      'name' => $term_name,
      'vid' => $vocabulary->vid,
      'google_product_taxonomy_id' => array(
        LANGUAGE_NONE => array(array('value' => $google_category_id)),
      ),
      'google_product_taxonomy_path' => array(
        LANGUAGE_NONE => array(array('value' => $term_path)),
      ),
    );
    // If we already created parent term.
    if (!empty($parent_path) && !empty($context['results']['tree'][$parent_path]['tid'])) {
      $term->parent = array($context['results']['tree'][$parent_path]['tid']);
    }
    taxonomy_term_save($term);

    // Add it to $context['results'] to make it available for further operations.
    $context['results']['tree'][$term_path]['tid'] = $term->tid;
  }
}

/**
 * Batch finish handler.
 */
function google_product_taxonomy_batch_import_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Import complete: <a href="@vocabulary_url">Google Product Taxonomy</a>.', array(
      '@vocabulary_url' => url('admin/structure/taxonomy/google_product_taxonomy'),
    ));
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}

/**
 * Batch processor: deletes taxonomy term.
 */
function google_product_taxonomy_batch_delete_process($tid, &$context) {
  taxonomy_term_delete($tid);
}

/**
 * Batch finish handler.
 */
function google_product_taxonomy_batch_delete_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message('Terms have been deleted successfully.');
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array(
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE)
    ));
    drupal_set_message($message, 'error');
  }
}
