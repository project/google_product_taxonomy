<?php

/**
 * Menu callback.
 *
 * @see google_product_taxonomy_menu()
 */
function google_product_taxonomy_import_form() {
  $form = array();

  $form['source'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Product Taxonomy source .txt file'),
    '#description' => t('URL of the <em>Taxonomy with numeric IDs in Plain Text (.txt)</em> source file to import Google Product Taxonomy from. See <a href="@url">The Google product taxonomy</a> page for more details.', array(
      '@url' => url('https://support.google.com/merchants/answer/1705911'),
    )),
    '#default_value' => url(GOOGLE_PRODUCT_TAXONOMY_SOURCE_URL),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#name' => 'import_taxonomy',
    '#value' => t('Import taxonomy'),
  );

  if ($vocabulary = taxonomy_vocabulary_machine_name_load(GOOGLE_PRODUCT_TAXONOMY_VOCABULARY_NAME)) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#name' => 'delete_terms',
      '#value' => t('Delete existing terms'),
    );
  }

  $form['#validate'] = array('google_product_taxonomy_import_form_validate');
  $form['#submit'] = array('google_product_taxonomy_import_form_submit');

  return $form;
}

/**
 * Form validation callback.
 *
 * @see google_product_taxonomy_import_form()
 * @see google_product_taxonomy_import_form_submit()
 */
function google_product_taxonomy_import_form_validate(&$form, &$form_state) {

}

/**
 * Form submission callback.
 *
 * @see google_product_taxonomy_import_form()
 * @see google_product_taxonomy_import_form_validate()
 */
function google_product_taxonomy_import_form_submit(&$form, &$form_state) {
  if ($form_state['clicked_button']['#name'] == 'import_taxonomy') {
    google_product_taxonomy_import_file($form_state['values']['source']);
  }
  elseif ($form_state['clicked_button']['#name'] == 'delete_terms') {
    google_product_taxonomy_delete_terms();
  }
}
